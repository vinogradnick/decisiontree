import pandas as pd
import numpy as np


"""
Разбиение X на 3 значения A,B,C
Тогда при разбиении исходного множества T по атрибуту X алгоритм
сформирует 3 потомка T1(a), T2(B)
"""


class Tree:
    def __init__(self, max_depth):
        self.max_depth = max_depth


class Node:
    def __init__(self, source, parent):
        self.source = source
        self.children = []
        self.parent = parent
        return

    @staticmethod
    def cut(source, feature_index):
        uniq_values = np.unique(source[feature_index])
