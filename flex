// The Round Table
digraph {
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" [label=odor]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" [label=odor]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "0551d8ed-8eea-11ea-8b77-ac2b6ed1be88"
	"0551d8ed-8eea-11ea-8b77-ac2b6ed1be88" [label=a shape=box]
	"0551d8ed-8eea-11ea-8b77-ac2b6ed1be88" [label=a]
	"0551d8ed-8eea-11ea-8b77-ac2b6ed1be88" -> "0551d8ec-8eea-11ea-9a09-ac2b6ed1be88"
	"0551d8ec-8eea-11ea-9a09-ac2b6ed1be88" [label="class-1 
 p=e"]
	"0551d8ec-8eea-11ea-9a09-ac2b6ed1be88" [label="class-1 
 p=e"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "0553115d-8eea-11ea-b451-ac2b6ed1be88"
	"0553115d-8eea-11ea-b451-ac2b6ed1be88" [label=c shape=box]
	"0553115d-8eea-11ea-b451-ac2b6ed1be88" [label=c]
	"0553115d-8eea-11ea-b451-ac2b6ed1be88" -> "0553115c-8eea-11ea-8cf8-ac2b6ed1be88"
	"0553115c-8eea-11ea-8cf8-ac2b6ed1be88" [label="class-1 
 p=p"]
	"0553115c-8eea-11ea-8cf8-ac2b6ed1be88" [label="class-1 
 p=p"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05550d2c-8eea-11ea-bc1b-ac2b6ed1be88"
	"05550d2c-8eea-11ea-bc1b-ac2b6ed1be88" [label=f shape=box]
	"05550d2c-8eea-11ea-bc1b-ac2b6ed1be88" [label=f]
	"05550d2c-8eea-11ea-bc1b-ac2b6ed1be88" -> "05550d2b-8eea-11ea-882f-ac2b6ed1be88"
	"05550d2b-8eea-11ea-882f-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05550d2b-8eea-11ea-882f-ac2b6ed1be88" [label="class-1 
 p=p"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05561ea3-8eea-11ea-a6ad-ac2b6ed1be88"
	"05561ea3-8eea-11ea-a6ad-ac2b6ed1be88" [label=l shape=box]
	"05561ea3-8eea-11ea-a6ad-ac2b6ed1be88" [label=l]
	"05561ea3-8eea-11ea-a6ad-ac2b6ed1be88" -> "05561ea2-8eea-11ea-bd4f-ac2b6ed1be88"
	"05561ea2-8eea-11ea-bd4f-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05561ea2-8eea-11ea-bd4f-ac2b6ed1be88" [label="class-1 
 p=e"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "0556bb1b-8eea-11ea-ae5c-ac2b6ed1be88"
	"0556bb1b-8eea-11ea-ae5c-ac2b6ed1be88" [label=m shape=box]
	"0556bb1b-8eea-11ea-ae5c-ac2b6ed1be88" [label=m]
	"0556bb1b-8eea-11ea-ae5c-ac2b6ed1be88" -> "0556bb1a-8eea-11ea-b778-ac2b6ed1be88"
	"0556bb1a-8eea-11ea-b778-ac2b6ed1be88" [label="class-1 
 p=p"]
	"0556bb1a-8eea-11ea-b778-ac2b6ed1be88" [label="class-1 
 p=p"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05ccae3c-8eea-11ea-b721-ac2b6ed1be88"
	"05ccae3c-8eea-11ea-b721-ac2b6ed1be88" [label=n shape=box]
	"05ccae3c-8eea-11ea-b721-ac2b6ed1be88" [label=n]
	"05ccae3c-8eea-11ea-b721-ac2b6ed1be88" -> "05b380e1-8eea-11ea-87d6-ac2b6ed1be88"
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" [label="spore-print-color"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" [label="spore-print-color"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b41d1d-8eea-11ea-8423-ac2b6ed1be88"
	"05b41d1d-8eea-11ea-8423-ac2b6ed1be88" [label=b shape=box]
	"05b41d1d-8eea-11ea-8423-ac2b6ed1be88" [label=b]
	"05b41d1d-8eea-11ea-8423-ac2b6ed1be88" -> "05b41d1c-8eea-11ea-93db-ac2b6ed1be88"
	"05b41d1c-8eea-11ea-93db-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b41d1c-8eea-11ea-93db-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b46b3a-8eea-11ea-ab59-ac2b6ed1be88"
	"05b46b3a-8eea-11ea-ab59-ac2b6ed1be88" [label=h shape=box]
	"05b46b3a-8eea-11ea-ab59-ac2b6ed1be88" [label=h]
	"05b46b3a-8eea-11ea-ab59-ac2b6ed1be88" -> "05b46b39-8eea-11ea-92de-ac2b6ed1be88"
	"05b46b39-8eea-11ea-92de-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b46b39-8eea-11ea-92de-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b50785-8eea-11ea-8f41-ac2b6ed1be88"
	"05b50785-8eea-11ea-8f41-ac2b6ed1be88" [label=k shape=box]
	"05b50785-8eea-11ea-8f41-ac2b6ed1be88" [label=k]
	"05b50785-8eea-11ea-8f41-ac2b6ed1be88" -> "05b50784-8eea-11ea-b291-ac2b6ed1be88"
	"05b50784-8eea-11ea-b291-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b50784-8eea-11ea-b291-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b5cad3-8eea-11ea-8d42-ac2b6ed1be88"
	"05b5cad3-8eea-11ea-8d42-ac2b6ed1be88" [label=n shape=box]
	"05b5cad3-8eea-11ea-8d42-ac2b6ed1be88" [label=n]
	"05b5cad3-8eea-11ea-8d42-ac2b6ed1be88" -> "05b5cad2-8eea-11ea-b7f0-ac2b6ed1be88"
	"05b5cad2-8eea-11ea-b7f0-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b5cad2-8eea-11ea-b7f0-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b618ec-8eea-11ea-ac55-ac2b6ed1be88"
	"05b618ec-8eea-11ea-ac55-ac2b6ed1be88" [label=o shape=box]
	"05b618ec-8eea-11ea-ac55-ac2b6ed1be88" [label=o]
	"05b618ec-8eea-11ea-ac55-ac2b6ed1be88" -> "05b618eb-8eea-11ea-99d0-ac2b6ed1be88"
	"05b618eb-8eea-11ea-99d0-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b618eb-8eea-11ea-99d0-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05b68e37-8eea-11ea-917d-ac2b6ed1be88"
	"05b68e37-8eea-11ea-917d-ac2b6ed1be88" [label=r shape=box]
	"05b68e37-8eea-11ea-917d-ac2b6ed1be88" [label=r]
	"05b68e37-8eea-11ea-917d-ac2b6ed1be88" -> "05b68e36-8eea-11ea-83d8-ac2b6ed1be88"
	"05b68e36-8eea-11ea-83d8-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05b68e36-8eea-11ea-83d8-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05cc11fb-8eea-11ea-bfd3-ac2b6ed1be88"
	"05cc11fb-8eea-11ea-bfd3-ac2b6ed1be88" [label=w shape=box]
	"05cc11fb-8eea-11ea-bfd3-ac2b6ed1be88" [label=w]
	"05cc11fb-8eea-11ea-bfd3-ac2b6ed1be88" -> "05c6e2d3-8eea-11ea-8548-ac2b6ed1be88"
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" [label=habitat]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" [label=habitat]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" -> "05c8ddc5-8eea-11ea-b141-ac2b6ed1be88"
	"05c8ddc5-8eea-11ea-b141-ac2b6ed1be88" [label=d shape=box]
	"05c8ddc5-8eea-11ea-b141-ac2b6ed1be88" [label=d]
	"05c8ddc5-8eea-11ea-b141-ac2b6ed1be88" -> "05c8b6a6-8eea-11ea-b4ca-ac2b6ed1be88"
	"05c8b6a6-8eea-11ea-b4ca-ac2b6ed1be88" [label="gill-size"]
	"05c8b6a6-8eea-11ea-b4ca-ac2b6ed1be88" [label="gill-size"]
	"05c8b6a6-8eea-11ea-b4ca-ac2b6ed1be88" -> "05c8b6a8-8eea-11ea-9533-ac2b6ed1be88"
	"05c8b6a8-8eea-11ea-9533-ac2b6ed1be88" [label=b shape=box]
	"05c8b6a8-8eea-11ea-9533-ac2b6ed1be88" [label=b]
	"05c8b6a8-8eea-11ea-9533-ac2b6ed1be88" -> "05c8b6a7-8eea-11ea-91c9-ac2b6ed1be88"
	"05c8b6a7-8eea-11ea-91c9-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05c8b6a7-8eea-11ea-91c9-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05c8b6a6-8eea-11ea-b4ca-ac2b6ed1be88" -> "05c8ddc4-8eea-11ea-a816-ac2b6ed1be88"
	"05c8ddc4-8eea-11ea-a816-ac2b6ed1be88" [label=n shape=box]
	"05c8ddc4-8eea-11ea-a816-ac2b6ed1be88" [label=n]
	"05c8ddc4-8eea-11ea-a816-ac2b6ed1be88" -> "05c8ddc3-8eea-11ea-b342-ac2b6ed1be88"
	"05c8ddc3-8eea-11ea-b342-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05c8ddc3-8eea-11ea-b342-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" -> "05c904c4-8eea-11ea-a5a4-ac2b6ed1be88"
	"05c904c4-8eea-11ea-a5a4-ac2b6ed1be88" [label=g shape=box]
	"05c904c4-8eea-11ea-a5a4-ac2b6ed1be88" [label=g]
	"05c904c4-8eea-11ea-a5a4-ac2b6ed1be88" -> "05c904c3-8eea-11ea-81d9-ac2b6ed1be88"
	"05c904c3-8eea-11ea-81d9-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05c904c3-8eea-11ea-81d9-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" -> "05cb9d43-8eea-11ea-b6e8-ac2b6ed1be88"
	"05cb9d43-8eea-11ea-b6e8-ac2b6ed1be88" [label=l shape=box]
	"05cb9d43-8eea-11ea-b6e8-ac2b6ed1be88" [label=l]
	"05cb9d43-8eea-11ea-b6e8-ac2b6ed1be88" -> "05cb75d1-8eea-11ea-9097-ac2b6ed1be88"
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" [label="cap-color"]
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" [label="cap-color"]
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" -> "05cb9d3c-8eea-11ea-8337-ac2b6ed1be88"
	"05cb9d3c-8eea-11ea-8337-ac2b6ed1be88" [label=c shape=box]
	"05cb9d3c-8eea-11ea-8337-ac2b6ed1be88" [label=c]
	"05cb9d3c-8eea-11ea-8337-ac2b6ed1be88" -> "05cb9d3b-8eea-11ea-95ae-ac2b6ed1be88"
	"05cb9d3b-8eea-11ea-95ae-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cb9d3b-8eea-11ea-95ae-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" -> "05cb9d3e-8eea-11ea-b359-ac2b6ed1be88"
	"05cb9d3e-8eea-11ea-b359-ac2b6ed1be88" [label=n shape=box]
	"05cb9d3e-8eea-11ea-b359-ac2b6ed1be88" [label=n]
	"05cb9d3e-8eea-11ea-b359-ac2b6ed1be88" -> "05cb9d3d-8eea-11ea-8abc-ac2b6ed1be88"
	"05cb9d3d-8eea-11ea-8abc-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cb9d3d-8eea-11ea-8abc-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" -> "05cb9d40-8eea-11ea-8cd6-ac2b6ed1be88"
	"05cb9d40-8eea-11ea-8cd6-ac2b6ed1be88" [label=w shape=box]
	"05cb9d40-8eea-11ea-8cd6-ac2b6ed1be88" [label=w]
	"05cb9d40-8eea-11ea-8cd6-ac2b6ed1be88" -> "05cb9d3f-8eea-11ea-885f-ac2b6ed1be88"
	"05cb9d3f-8eea-11ea-885f-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05cb9d3f-8eea-11ea-885f-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05cb75d1-8eea-11ea-9097-ac2b6ed1be88" -> "05cb9d42-8eea-11ea-8faa-ac2b6ed1be88"
	"05cb9d42-8eea-11ea-8faa-ac2b6ed1be88" [label=y shape=box]
	"05cb9d42-8eea-11ea-8faa-ac2b6ed1be88" [label=y]
	"05cb9d42-8eea-11ea-8faa-ac2b6ed1be88" -> "05cb9d41-8eea-11ea-ac9c-ac2b6ed1be88"
	"05cb9d41-8eea-11ea-ac9c-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05cb9d41-8eea-11ea-ac9c-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" -> "05cbc3f8-8eea-11ea-9da6-ac2b6ed1be88"
	"05cbc3f8-8eea-11ea-9da6-ac2b6ed1be88" [label=p shape=box]
	"05cbc3f8-8eea-11ea-9da6-ac2b6ed1be88" [label=p]
	"05cbc3f8-8eea-11ea-9da6-ac2b6ed1be88" -> "05cbc3f7-8eea-11ea-aa59-ac2b6ed1be88"
	"05cbc3f7-8eea-11ea-aa59-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cbc3f7-8eea-11ea-aa59-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05c6e2d3-8eea-11ea-8548-ac2b6ed1be88" -> "05cc11fa-8eea-11ea-9f6a-ac2b6ed1be88"
	"05cc11fa-8eea-11ea-9f6a-ac2b6ed1be88" [label=w shape=box]
	"05cc11fa-8eea-11ea-9f6a-ac2b6ed1be88" [label=w]
	"05cc11fa-8eea-11ea-9f6a-ac2b6ed1be88" -> "05cc11f9-8eea-11ea-ab40-ac2b6ed1be88"
	"05cc11f9-8eea-11ea-ab40-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05cc11f9-8eea-11ea-ab40-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05b380e1-8eea-11ea-87d6-ac2b6ed1be88" -> "05ccae3b-8eea-11ea-b4db-ac2b6ed1be88"
	"05ccae3b-8eea-11ea-b4db-ac2b6ed1be88" [label=y shape=box]
	"05ccae3b-8eea-11ea-b4db-ac2b6ed1be88" [label=y]
	"05ccae3b-8eea-11ea-b4db-ac2b6ed1be88" -> "05ccae3a-8eea-11ea-920a-ac2b6ed1be88"
	"05ccae3a-8eea-11ea-920a-ac2b6ed1be88" [label="class-1 
 p=e"]
	"05ccae3a-8eea-11ea-920a-ac2b6ed1be88" [label="class-1 
 p=e"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05ce0dc2-8eea-11ea-8e30-ac2b6ed1be88"
	"05ce0dc2-8eea-11ea-8e30-ac2b6ed1be88" [label=p shape=box]
	"05ce0dc2-8eea-11ea-8e30-ac2b6ed1be88" [label=p]
	"05ce0dc2-8eea-11ea-8e30-ac2b6ed1be88" -> "05ce0dc1-8eea-11ea-b3ee-ac2b6ed1be88"
	"05ce0dc1-8eea-11ea-b3ee-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05ce0dc1-8eea-11ea-b3ee-ac2b6ed1be88" [label="class-1 
 p=p"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05cf6d5b-8eea-11ea-8ef3-ac2b6ed1be88"
	"05cf6d5b-8eea-11ea-8ef3-ac2b6ed1be88" [label=s shape=box]
	"05cf6d5b-8eea-11ea-8ef3-ac2b6ed1be88" [label=s]
	"05cf6d5b-8eea-11ea-8ef3-ac2b6ed1be88" -> "05cf6d5a-8eea-11ea-b14c-ac2b6ed1be88"
	"05cf6d5a-8eea-11ea-b14c-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05cf6d5a-8eea-11ea-b14c-ac2b6ed1be88" [label="class-1 
 p=p"]
	"054f4101-8eea-11ea-86fd-ac2b6ed1be88" -> "05d0ccea-8eea-11ea-b889-ac2b6ed1be88"
	"05d0ccea-8eea-11ea-b889-ac2b6ed1be88" [label=y shape=box]
	"05d0ccea-8eea-11ea-b889-ac2b6ed1be88" [label=y]
	"05d0ccea-8eea-11ea-b889-ac2b6ed1be88" -> "05d0cce9-8eea-11ea-b60e-ac2b6ed1be88"
	"05d0cce9-8eea-11ea-b60e-ac2b6ed1be88" [label="class-1 
 p=p"]
	"05d0cce9-8eea-11ea-b60e-ac2b6ed1be88" [label="class-1 
 p=p"]
}
