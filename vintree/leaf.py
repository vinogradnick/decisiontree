import uuid


class Leaf:
    def __init__(self, label, decision):
        self.propably = label
        self.decision = decision
        self.children = None
        self.uuid = str(uuid.uuid1())
        self.label = "class-{} \n p={}".format(label, decision)


class Attribute:
    def __init__(self, label, children):
        self.label = label
        self.children = [children]
        self.uuid = str(uuid.uuid1())

    def __str__(self):
        return "attr {}".format(self.label)
