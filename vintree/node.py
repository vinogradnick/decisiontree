from vintree.entropy import GainEntropy
from vintree.feature import Feature
from vintree.support import filter, propably
import vintree.entropy as ent
import numpy as np
import uuid


class Node:
    def __init__(self, label='', feature=None):
        """
        Узел дерева включающий в себя информацию
        по метке, энтропийному критерию
        """
        self.label = label
        self.feature = feature
        self.children = []
        self.uuid = str(uuid.uuid1())


"""

 def split(self, level: int = 0):

        root_feature = self.cut_features(
            self.source, self.class_values, self.columns)
        #Новый узел
        new_node = Node(self.source, root_feature,
                        self.class_values, self.columns)

   

    def get_status(self):
        classes = np.unique(self.class_values)
        if len(classes) == 1:
            return "{} \n propably:{:.3f}".format(classes[0], 1)

        pos = propably(self.class_values, classes[0])
        neg = propably(self.class_values, classes[1])
        return "{} \n propably:{:.3f}".format(classes[0], pos) if pos > neg else "{} \n propably:{:.3f}".format(classes[1], neg)


def remove_column(self, source: np.array, index: int):
        return np.delete(source.transpose(), index, 0).transpose()


 if level > 4:
            return self
        feature_values = np.unique(self.source.transpose()[root_feature.index])
        for feature_value in feature_values:
            node_values = []
            node_class_values = []
            for row_index in range(len(self.source)):
                # Если значения в массиве source === feature
                if self.source[row_index][root_feature.index] == feature_value:
                    # Добавляем значение класса в узел
                    node_class_values.append(self.class_values[row_index])
                    # Выполняем удаление колонки
                    newRow = self.remove_column(
                        self.source[row_index], root_feature.index)
                    # Добавляем колонку в массив значений
                    node_values.append(newRow)
            # Cоздаем новый узел и записываем значения
            new_node = Node(np.array(node_values),
                            root_feature.label+'-' +
                            feature_value, np.array(node_class_values),
                            self.remove_column(
                                self.columns, root_feature.index),
                            self)
            # Добавляем новый узел в массив узлов
            self.node_children.append(new_node)
        return self


"""
