import numpy as np
import vintree.support as st
import math as m


class Algorithm:
    def operation(self, decision: np.array, features: np.array) -> float:
        return 1.0


class GainEntropy(Algorithm):

    def operation(self, decision: np.array, features: np.array) -> float:
        """
        Вычисление однородности образца. Если образец полностью однороден, то энтропия равна нулю, а если образец разделен поровну, то энтропия равна единице.
        """
        return self.gain_ratio(decision, features)

    def entropy(self, decision: np.array) -> float:
        """
        Вычисление энтропии для элементов выборки
        """
        sum = 0
        for feature in np.unique(decision):
            feature_propably = st.propably(decision, feature)
            sum = sum-self.entropy_part(feature_propably)
        return sum

    def entropy_part(self, propably: float) -> float:
        """
        Вычисление энтропии для одного элемента выборки
        """
        return propably*m.log2(propably)

    def gain_ratio(self, decision: np.array, features: np.array) -> float:
        """
        Прирост информации основан на уменьшении энтропии после разделения набора данных на атрибут. Построение дерева решений-это поиск атрибута, который возвращает наибольший информационный выигрыш (т. е. наиболее однородные ветви).
        """
        gain_value = self.entropy(decision)
        for attribute in np.unique(features):
            gain_value = gain_value - \
                self.gain_part(decision, features, attribute)
        return gain_value

    def gain_part(self, decision, features, attribute):
        filtered_features = st.filter_by_tdarray(decision, features, attribute)
        attribute_entropy = self.entropy(filtered_features)
        return st.propably(features, attribute) * attribute_entropy
