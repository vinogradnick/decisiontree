from sklearn.tree import DecisionTreeClassifier
import pandas as pd
import numpy as np


class RandomForest:
    def __init__(self, len_tree):
        self.len_tree = len_tree

    def train(self, X_train, Y_train):
        forest = []
        for i in range(self.len_tree):
            x_fraq = X_train.sample(frac=1, random_state=777)
            y_fraq = Y_train.sample(frac=1, random_state=777)
            tree = DecisionTreeClassifier()  # Sklearn base tree model
            tree.fit(x_fraq.values, y_fraq.values)
            forest.append(tree)
            print('train '+str(i))
        return forest

    def accurancy(self, Y):
        pred = []
        for row in Y:
            tr = 0
            for value in row:
                if value == 0:
                    tr += 1
            pred.append(tr/len(row))
        return pred

    @staticmethod
    def predict(model, x_test):
        predictions = []
        for i in range(len(model)):
            predictions.append(model[i].predict(x_test))
        return predictions
