from vintree.support import filter, propably, filter_by_tdarray, frequency
from vintree.entropy import GainEntropy
from vintree.feature import Feature
from vintree.node import Node
from vintree.leaf import Leaf, Attribute
import vintree.entropy as ent
import numpy as np
import uuid


class DecisionTree:
    def __init__(self, columns, max_depth=10, lef_size=20):
        self.algorithm = GainEntropy()
        self.max_depth = max_depth
        self.leaf_maxsize = lef_size
        self.columns = columns

    def split(self, source: np.array, decisions: np.array, depth):
        unique_decisions = np.unique(decisions)

        if len(unique_decisions) == 1:
            return Leaf(1, unique_decisions[0])

        if len(decisions) <= self.leaf_maxsize:
            return self.stop(decisions, unique_decisions)

        if depth >= self.max_depth:
            return self.stop(decisions, unique_decisions)

        feature = Feature.cut(GainEntropy(), source, decisions, self.columns)

        if feature.parameter == 0:
            return self.stop(decisions, unique_decisions)

        node = Node(feature.label, feature)
        # Получаем список атрибутов которые будут использованы
        attributes = np.unique(source.transpose()[feature.index])
        # Получаем количество уникальных решений

        # print("-------------------------------------------------------------")
        # print(feature)
        # print("-------------------------------------------------------------")
        for attribute in attributes:
            # print("edge={}".format(attribute))
            # Строки которые подходят
            attribute_source = []
            # Решения для аттрибута
            atrribute_decisions = []
            # Выбираем все решения и строки в примеры которые принадлежат значениям атрибута
            for source_index in range(len(source)):
                if source[source_index][feature.index] == attribute:
                    attribute_source.append(source[source_index])
                    atrribute_decisions.append(decisions[source_index])
            # Перебираем значения для определения к какому типу относится
            if len(atrribute_decisions) != 0:
                node.children.append(Attribute(attribute, self.split(np.array(attribute_source),
                                                                     np.array(atrribute_decisions), depth+1)))
        return node

    def stop(self, decisions, unique_decisions):
        pos = len(filter(decisions, unique_decisions[0]))
        neg = len(filter(decisions, unique_decisions[1]))
        value = pos/(pos+neg)
        if pos > neg:
            return Leaf(value, unique_decisions[0])
        else:
            return Leaf(1-value, unique_decisions[1])

    def train(self, X, Y):
        return self.split(X, Y, 0)

    @staticmethod
    def predict(model, X: np.ndarray):
        arr = [DecisionTree.get_ac(model, row) for row in X]
        return arr

    @staticmethod
    def accurancy(Y1, Ypredicted):
        tp = 0

        for pred in range(len(Y1)):
            if Y1[pred] == Ypredicted[pred]:
                tp += 1
        return tp/len(Y1)

    @staticmethod
    def get_ac(model, row):

        if isinstance(model, Leaf):
            return model.decision

        if isinstance(model, Node):
            column_index = model.feature.index
            # Перебираем атрибуты
            for child in model.children:
                if child is not None:
                    if child.label == row[column_index]:
                        # Нашли атрибут
                        return DecisionTree.get_ac(child, row)
        if isinstance(model, Attribute):
            for child in model.children:
                if isinstance(child, Leaf):
                    return child.decision
                else:
                    if isinstance(child, Node):
                        return DecisionTree.get_ac(child, row)
