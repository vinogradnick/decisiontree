import numpy as np


def frequency(array: np.array, features: np.array) -> float:
    """
    Получение частоты встречаемости показателя
    в массиве
    """
    return len(features)/len(array)


def propably(array: np.array, feature) -> float:
    """
    Получение вероятности встречи
    опеределенного класса из общей выборки
    """
    positive_values = filter(array, feature)
    propably_value = frequency(array, positive_values)
    return propably_value


def filter(array: np.array, feature) -> np.array:
    """
    Фильтрация массива данных для
    нахождения элементов соответствующему заданому как параметр данной функции
    """
    return np.array([feature_value for feature_value in array if feature_value == feature])


def filter_by_tdarray(source: np.array, features: np.array, param) -> np.array:
    """
    Фильтрация данных одного массива 
    на основании данных другого массива
    """
    return np.array([source[i] for i in range(len(source)) if features[i] == param])

def mode(source: np.array) -> np.array:
    """
    Получение самых популярных значений
    """
    