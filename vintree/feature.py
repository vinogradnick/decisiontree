import vintree.support as st
import vintree.entropy as ent
import numpy as np


class Feature:
    def __init__(self, index, label, parameter):
        self.index = index
        self.label = label
        self.parameter = parameter

    def __str__(self):
        return "feature| label:{} \t/ index:{} \t/ parameter:{}".format(self.label, self.index, self.parameter)

    @staticmethod
    def split(algorithm: ent.GainEntropy, source: np.array, labels: np.array, decisions: np.array):
        """
        Разбиение выборки на фичи
        """
        feature_array = []
        # Транспанируем массив для получения всех значений в строки
        transposed_source = source.transpose()
       
        for row_index in range(len(transposed_source)):
            # Вычисляем прирост информации
            feature_gain = algorithm.operation(
                decisions, transposed_source[row_index])

            # Создаем фичу
            feature = Feature(row_index, labels[row_index], feature_gain)
            # Добавляем фичу в массив
            feature_array.append(feature)

        return np.array(feature_array)

    @staticmethod
    def sort(labels: np.array) -> np.array:
        """
        Сортировка элементов массива по параметру фичи
        в порядке убывания
        """
        return np.array(sorted(labels, key=lambda feature: feature.parameter, reverse=True))

    @staticmethod
    def cut(algorithm: ent.GainEntropy, source: np.array, decisions: np.array, columns: np.array):
        result = Feature.split(algorithm, source,
                               columns, decisions)
        feature = Feature.sort(result)[0]
        return feature
