from vintree.entropy import GainEntropy
from vintree.support import filter_by_tdarray
import numpy as np
import unittest


class GainEntropyTest(unittest.TestCase):
    def setUp(self):
        self.values = np.array(
            [-1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1])
        self.wind_classes = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]

    def test_entropy(self):
        entropyAlgorithm = GainEntropy()
        filter_weaks = filter_by_tdarray(self.values, self.wind_classes, 0)
        print(filter_weaks)
        self.assertEqual(entropyAlgorithm.entropy(
            filter_weaks), 0.8112781244591328, "etropy value")

    def test_gain(self):
        entropyAlgorithm = GainEntropy()
        gain_ratio = entropyAlgorithm.operation(
            self.values, self.wind_classes)
        self.assertEqual(gain_ratio, 0.048127030408269544)




if __name__ == '__main__':
    unittest.main()
