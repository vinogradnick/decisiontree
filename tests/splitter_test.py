from vintree.entropy import GainEntropy
from vintree.support import filter_by_tdarray
from vintree.feature import Feature
from vintree.splitter import IdModel
from vintree.leaf import Attribute, Leaf
from vintree.node import Node
from graphviz import Digraph
import pandas as pd
import numpy as np
import unittest
import uuid


class ID3Test(unittest.TestCase):
    def setUp(self):
        df = pd.read_csv(
            "C:/Users/vinogradik/YandexDisk/prav/vinograd/examples/mushrooms.csv")
      
        self.decisions = df['class'].values
        
        df = df.drop(['class'], axis=1)
        self.source = df.values
        self.columns = df.columns

    def test_id3(self):
        id3 = IdModel(self.columns)
        res = id3.split(self.source, self.decisions, 0)
        print(res.label)
        print('-----------------------------------')
        dot = Digraph(comment='The Round Table')
        dot.node(res.uuid, res.label)
        self.printer(res, dot)
        dot.render('flex')

        self.assertEqual(True, True)

    def printer(self, node, dot):
        dot.node(node.uuid, node.label)
        if node.children is not None:
            for child in node.children:
                if child is not None:
                    if type(child) == Attribute:
                        dot.edge(node.uuid, child.uuid)
                        dot.node(child.uuid, child.label, shape='box')
                    else:
                        dot.edge(node.uuid, child.uuid)
                        dot.node(child.uuid, child.label)
                self.printer(child, dot)

    def test_train(self):
        id3 = IdModel(self.columns)
        res = id3.train(self.source[:7500], self.decisions[:7500])
        res = IdModel.predict(res, self.source[7500:])
        nres = [i for i in res if i is not None]

        ac = IdModel.accurancy(nres, self.decisions[7500:])
        print(ac)


if __name__ == '__main__':
    unittest.main()

"""


  # def setUp(self):
    #     self.df = pd.read_csv(
    #         "C:/Users/vinogradik/YandexDisk/prav/vinograd/examples/mushrooms.csv")
    #     self.class_values = self.df['class'].values
    #     self.df = self.df.drop(['class'], axis=1)
    #     self.column_headers = self.df.columns
    #     self.values = self.df.values
"""
