from vintree.entropy import GainEntropy
from vintree.support import filter_by_tdarray
from vintree.feature import Feature
import pandas as pd
import numpy as np
import unittest


class FeatureClassTest(unittest.TestCase):
    def setUp(self):
        self.df = pd.read_csv(
            "C:/Users/vinogradik/YandexDisk/prav/vinograd/examples/mushrooms.csv")
        

    def test_cut(self):
        algorithm = GainEntropy()
        feature = Feature.cut(algorithm, self.values,
                              self.class_values, self.labels)
        print(feature)
        self.assertEqual(feature.parameter, 0.9060749773839999)


if __name__ == '__main__':
    unittest.main()
