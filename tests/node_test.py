from vintree.entropy import GainEntropy
from vintree.support import filter_by_tdarray
from vintree.feature import Feature
from vintree.node import Node
from graphviz import Digraph
import pandas as pd
import numpy as np
import unittest
import uuid


class NodeSplitTest(unittest.TestCase):
    def setUp(self):
        self.df = pd.read_csv(
            "C:/Users/vinogradik/YandexDisk/prav/vinograd/examples/mushrooms.csv").iloc[:, :300]
        self.class_values = self.df['class'].values
        self.df = self.df.drop(['class'], axis=1)
        self.column_headers = self.df.columns
        self.values = self.df.values

    def test_split(self):
        dot = Digraph(comment='The Round Table')
        node = Node(self.values, "root", self.class_values,
                    self.column_headers, None)
        res = node.split()
        print(node.class_values)
        dot.node(res.uuid, res.label)
        self.render_next(res.node_children, dot)
    
        dot.render('flex')
        self.assertTrue(True, "end")

    def render_next(self, children: np.array, dot: Digraph):
        for child in children:
            print(child.label)
            print(child.class_values)
            dot.node(child.uuid, child.label)
            dot.edge(child.parent.uuid, child.uuid)
            if len(child.node_children) > 0:
                self.render_next(child.node_children, dot)
            else:
                uuid_eval = str(uuid.uuid1())
                dot.node(uuid_eval, child.get_status())
                dot.edge(child.uuid, uuid_eval)

    def render(self, node: Node, graph: Digraph):
        graph.node(node.uuid, node.label)
        if node.parent is not None:
            graph.edge(node.parent.uuid, node.uuid)


if __name__ == '__main__':
    unittest.main()
